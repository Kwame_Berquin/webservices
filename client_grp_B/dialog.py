#!/usr/bin/env python
#This is the user-dialog component of a client implementing webservice grp B
import communicator
import helpers
import stringify

#global hostname
host = ""


#the main menue, posting all possible options
def main_menue():
    print '\n-------Main Menue-------\n'
    print 'Options:\n', \
        '1 read a poll as normal user\n', \
        '2 read a poll as admin\n', \
        '3 create a new poll\n', \
        '4 edit an existing poll\n', \
        '5 delete a poll\n', \
        '6 vote on a poll\n', \
        '7 edit a vote\n', \
        '8 delete a vote\n', \
        'q quit\n'
    option = raw_input("Please enter your choice: ")
    #wannabe switch
    if option == "1":
        poll_get_normal(-1)
    return

#reads a poll with the given id. If the id is -1, prompts the user to enter a id            
def poll_get_normal(id):
    print '\n-------Read Poll-------\n'
    if id == -1:
        id = raw_input("Please enter the poll you want to read (normal id): ")

    response = communicator.poll_get_normal(host, id)
    if response["status"] == "200":
        stringify.poll(response["poll"])
    else:
        print 'Could not find poll! Server retuned: ' + str(response["status"]) + " " + response["reason"]
    return

if __name__ == "__main__":
    print 'The is a client implementing webservice group B.'
    host = raw_input("Please enter the host URL and port: ")
    main_menue()