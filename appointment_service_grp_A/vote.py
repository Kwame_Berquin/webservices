#!/usr/bin/env python
import web
import simplejson as json
import helpers

#connect to DB
db = web.database(dbn='sqlite', db='appointmentDB')

#operations on /poll/ID/vote
class vote:  
    def POST(self, pollID):
        #check if poll exists
        if not helpers.poll_exist(pollID):
            web.ctx.status = '401 no such poll'
            return
        #read request body
        try:
            vote = json.loads(web.data())
        except:
            web.ctx.status = '400 body JSON format faulty'
            return
        
        #check input for validity
        if not helpers.isset(vote, "name"):
            vote["name"] = ""
        if not helpers.isset(vote, "appointments"):
            vote["appointments"] = []
        for appointment in vote["appointments"]:
            if not helpers.isset(appointment, "id") or not helpers.appointment_exists(pollID, appointment["id"]):
                web.ctx.status = '404 no such appointment'
                return
        
        #generate editkey
        ret = {}
        ret["editkey"] = helpers.create_key()
        
        #save vote
        ret["id"] = db.insert('vote', name=vote["name"], editkey=ret["editkey"], FK_poll=pollID)
        for appointment in vote["appointments"]:
            db.insert('vote_on_appointment', FK_vote=ret["id"], FK_appointment=appointment["id"], FK_poll=pollID)
        
        #return
        return json.dumps(ret, indent=4, separators=(',', ': '))
    
