#!/usr/bin/env python
import web
from polls import polls
from poll import poll
from vote import vote
from votes import votes

urls = (
    '/poll', 'polls',
    '/poll/([0-9.]*)', 'poll',
    '/poll/([0-9]*)/vote', 'vote',  
    '/poll/([0-9]*)/vote/([0-9]*)', 'votes' 
)

app = web.application(urls, globals())

if __name__ == "__main__":
    app.run()
