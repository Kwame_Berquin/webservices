#!/usr/bin/env python
import web
import simplejson as json
from helpers import create_key

#connect to DB
db = web.database(dbn='sqlite', db='appointmentDB')

#operations on /poll
class polls:     
    #get all polls   
    def GET(self):
        polls = db.query("SELECT * FROM poll")
        return json.dumps(polls.list(), indent=4, separators=(',', ': '))
        
    #creates a poll and appointments
    def POST(self):
        #read request body
        try:
            poll = json.loads(web.data())
        except:
            web.ctx.status = '400 body JSON format faulty'
            return
        #check input for validity
        try:
            poll["name"]
            poll["appointments"]
            for appointment in poll["appointments"]:
                appointment["start"]
                appointment["duration"]
        except KeyError:
            web.ctx.status = '401 missing information' #poll not complete
        else:
            #input valid. write to db
            ret = {}
            ret["editkey"] = create_key()
            #insert poll
            ret["id"] = db.insert('poll', name=poll["name"], editkey=ret["editkey"])
            #insert appointments
            for appointment in poll["appointments"]:
                db.insert('appointment', FK_poll=ret["id"], start=appointment["start"], duration=appointment["duration"])
            return json.dumps(ret, indent=4, separators=(',', ': '))
