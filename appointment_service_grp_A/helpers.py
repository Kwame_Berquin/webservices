#!/usr/bin/env python
import string 
import random
import web

#connect to DB
db = web.database(dbn='sqlite', db='appointmentDB')

#generate a 16 digit random String
def create_key():
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in range(16))

#checks, if a key is set for an object
def isset(obj, key):
    try:
        obj[key]
    except KeyError:
        return False
    else:
        return True
    
#does a poll exist?
def poll_exist(id):
    return len(db.query("SELECT * FROM poll WHERE id="+str(id)).list()) == 1
    
#does a appointment for a poll exist?
def appointment_exists(pollID, appointmentID):
    return len(db.query("SELECT * FROM appointment WHERE id=" + str(appointmentID) + " and FK_poll=" + str(pollID)).list()) == 1

#does a vote exist?
def vote_exist(voteID):
    return len(db.query("SELECT * FROM vote WHERE id=" + str(voteID)).list()) == 1
