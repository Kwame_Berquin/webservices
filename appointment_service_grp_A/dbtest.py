#!/usr/bin/env python
import web
import simplejson as json

#connect to DB
db = web.database(dbn='sqlite', db='appointmentDB')

polls = db.query("SELECT * FROM poll")
#print polls[0].name # -> prints number of entries in 'users' table
#print json.dumps(polls[0])
print json.dumps(polls.list())
